const m = require('mithril');
const HomeLayout = require('./Home');
const CounterLayout = require('./Counter').layout;
const TimerLayout = require('./Timer').component;
const TabataConfigure = require('./Tabata').config;
const TabataTimer = require('./Tabata').timer;
const WeightLayout = require('./Weight').layout;

let App = {
    init: function() {
        let app_root = document.querySelector('main#app');
        m.route(app_root, '/', {
            "/": HomeLayout,
            "/counter": CounterLayout,
            "/timer/:timer_seconds": TimerLayout,
            "/tabata/configure": TabataConfigure,
            "/tabata/:timer_config": TabataTimer,
            "/weight": WeightLayout
        });
    }
};

module.exports = App;
