const m = require('mithril');
const Navbar = require('./Navbar');

class CounterComponent {
    constructor(vnode) {
        this.whats_counted = vnode.attrs.what[0].toUpperCase() + 
                             vnode.attrs.what.slice(1);
        if (sessionStorage.getItem(this.whats_counted)) {
            this.count = parseInt(sessionStorage.getItem(this.whats_counted), 10);
        } else {
            this.count = 0;
            sessionStorage.setItem(this.whats_counted, 0);
        }
    }

    setCountInStorage() {
        sessionStorage.setItem(this.whats_counted, this.count);
    }

    increment() {
        this.count += 1;
        this.setCountInStorage();
        if (this.whats_counted === 'Sets') {
            m.route.set('/timer/:timer_seconds', { timer_seconds: 60 });
        }
    }

    reset() {
        this.count = 0;
        this.setCountInStorage();
    }

    view(vnode) {
        return m('section.Counter.container', [
            m('h1.title.is-4', `${this.whats_counted} completed: ${this.count}`),
            m('.level.is-mobile', [
                m('a.button.is-primary.is-large.level-item', {
                    onclick: () => this.increment()
                }, '+1'),
                m('a.button.is-danger.is-large.level-item', {
                    onclick: () => this.reset()
                }, 'reset')
            ])
        ]);
    }
}

class CounterLayout {
    constructor(vnode) {}

    view(vnode) {
        return [
            m(Navbar, { current: 'counter' }),
            m('section.section', [
                m(CounterComponent, { what: 'reps' }),
                m('br'),
                m(CounterComponent, { what: 'sets' }),
                m('br'),
                m('br'),
                m('a.button.is-fullwidth.is-info.is-large', {
                    onclick: () => {
                        sessionStorage.clear();
                        m.route.set('/');
                    }
                }, 'Finish Workout')
            ])
        ];
    }
}

module.exports = { component: CounterComponent, layout: CounterLayout };
