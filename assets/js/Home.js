const m = require('mithril');
const Navbar = require('./Navbar');

class HomeLayout {
    view(vnode) {
        return [
            m(Navbar, { current: 'home' }),
            m('section.section', [
                m('.container', [
                    m('a.button[href=#!/counter].is-fullwidth.is-large.is-info', {
                        oncreate: m.route.link,
                        onclick: () => {
                            sessionStorage.clear();
                            m.route.set('#!/counter');
                        }
                    }, 'Workout Counter'),
                    m('br'),
                    m('a.button[href=#!/tabata/configure].is-fullwidth.is-large.is-info', {
                        oncreate: m.route.link
                    }, 'Tabata Timer'),
                    m('br'),
                    m('a.button[href=#!/weight].is-fullwidth.is-large.is-info', {
                        oncreate: m.route.link
                    }, 'Weight Tracker'),
                ])
            ])
        ];
    }
}

module.exports = HomeLayout;
