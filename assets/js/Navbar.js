const m = require('mithril');

class Navbar {
    toggleMenu() {
        let burger = document.querySelector('a.navbar-burger');
        let menu = document.querySelector('.navbar-menu');
        burger.classList.toggle('is-active');
        menu.classList.toggle('is-active');
    }

    view(vnode) {
        return [
            m('nav.navbar[role=navigation][aria-label="main navigation"]', [
                m('.navbar-brand', [
                    m('a.navbar-item[href=/]', {
                        oncreate: m.route.link
                    }, 'total body'),
                    m('a.navbar-burger[role=button][aria-label=menu][aria-expanded=false]', {
                        onclick: () => this.toggleMenu()
                    }, [
                        m('span[aria-hidden=true]'),
                        m('span[aria-hidden=true]'),
                        m('span[aria-hidden=true]')
                    ])
                ]),
                m('.navbar-menu', [
                    m('.navbar-start', [
                        m('a.navbar-item[href=/]', {
                            oncreate: m.route.link,
                            class: vnode.attrs.current_page === 'home' ? 'is-active' : ''
                        }, 'Home')
                    ]),
                    m('.navbar-end', [
                        m('.navbar-item.has-dropdown', [
                            m('a.navbar-link', 'Tools'),
                            m('.navbar-dropdown', [
                                m('a.navbar-item[href=/counter]', {
                                    oncreate: m.route.link,
                                    class: vnode.attrs.current_page === 'counter' ? 'is-active' : ''
                                }, 'Counter'),
                                m('a.navbar-item[href=/tabata/configure]', {
                                    oncreate: m.route.link,
                                    class: vnode.attrs.current_page === 'tabata' ? 'is-active' : ''
                                }, 'Tabata Timer'),
                                m('a.navbar-item[href=/weight]', {
                                    oncreate: m.route.link,
                                    class: vnode.attrs.current_page === 'weight' ? 'is-active' : ''
                                }, 'Weight Tracker')
                            ])
                        ]),
                        m('a.navbar-item', {
                            onclick: () => {
                                let message = "Version: 0.5\n\n(c)2019, John Boisselle";
                                navigator.notification.alert(message, null, 'About: Total Body', 'OK');
                            }
                        }, 'About')
                    ])
                ])
            ])
        ];
    }
}

module.exports = Navbar;
