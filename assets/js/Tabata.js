const m = require('mithril');
const Navbar = require('./Navbar');

class TabataTimer {
    constructor(vnode) {}

    view() {

    }
}

class TabataConfigure {
    constructor(vnode) {}

    view() {
        return [
            m(Navbar, { current: 'tabata' }),
            m('h1', 'Configure Screen')
        ];
    }
}

module.exports = { timer: TabataTimer, config: TabataConfigure }
