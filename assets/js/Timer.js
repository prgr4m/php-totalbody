const m = require('mithril');
const Navbar = require('./Navbar');

class TimerComponent {
    constructor(vnode) {
        // this.duration = 60 * 1; // need 2 minutes? then x's 2
        // this.timer = 60 * 1;
        this.timer = vnode.attrs.timer_seconds || 60;
        this.timer_state = vnode.attrs.timer_state || 'start';
        this.text_display = this.setTextDisplay();
    }

    setTextDisplay() {
        let minutes = parseInt(this.timer / 60, 10);
        let seconds = parseInt(this.timer % 60, 10);
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        return `${minutes}:${seconds}`;
    }

    oncreate(vnode) {
        this.interval_handle = window.setInterval(() => this.countdown(), 1000);
    }

    countdown() {
        this.text_display = this.setTextDisplay();
        this.timer--;
        if(this.timer < 0) {
            window.clearInterval(this.interval_handle);
            m.route.set('/counter');
        } else {
            m.redraw();
        }
    }

    view() {
        return m('section.TimerComponent.box', [
                   m('.countdown_display.has-text-centered', [
                       m('h1.title', 'Take a break!'),
                       m('p.title', `${this.text_display}`)
                   ]),
                   m('br'),
                   m('a.button[href=/counter].is-fullwidth.is-danger', {
                       oncreate: m.route.link,
                       onclick: (event) => {
                           event.preventDefault();
                           window.clearInterval(this.interval_handle);
                           m.route.set('/counter');
                       }
                   }, 'stop')
               ]);
    }
}

class TimerLayout {
    constructor(vnode) {
        this.timer_seconds = vnode.attrs.timer_seconds || 0;
        this.timer_state = vnode.attrs.timer_state || 'paused';
    }

    view(vnode) {
        return [
            m(Navbar, { current: 'timer' }),
            m(TimerComponent, { 
                timer_seconds: this.timer_seconds,
                timer_state: this.timer_state
            })
        ];
    }
}

module.exports = { component: TimerLayout };
