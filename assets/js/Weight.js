const m = require('mithril');
const Navbar = require('./Navbar');

// ADD | HISTORY | PROGRESS

class WeightLayout {
    constructor(vnode) {
        this.active_tab = 'add_weight';
    }

    toggleTab() {
        this.active_tab = (this.active_tab === 'add_weight') ? 'progress_chart' : 'add_weight';
    }

    view(vnode) {
        return [
            m(Navbar, { current: 'weight' }),
            m('br'),
            m('.tabs.is-toggle.is-centered', [
                m('ul', [
                    m('li', {
                        onclick: () => this.toggleTab(),
                        class: this.active_tab === 'add_weight' ? 'is-active' : ''
                    }, m('a', 'Add Weight')),
                    m('li', {
                        onclick: () => this.toggleTab(),
                        class: this.active_tab === 'progress_chart' ? 'is-active' : ''
                    }, m('a', 'Weight History')),
                   
                ])
            ]),
            this.active_tab === 'add_weight' ? m(InputComponent) : m(HistoryLayout)
        ];
    }
}


class ProgressChart {
    constructor(vnode) {
        this.dataset = [];
    }

    oninit(vnode) {
        // make request to database since I'm cutting out a web service in this port
        // m.request({
        //     url: 
        // }).then((data) => {
        //
        // });
    }

    initChart() {
        // check to make sure there is data (and enough data) 
        // to plot out in a chart (was using chartist but may try something else)
    }

    view(vnode) {
        return [
            m('h1', 'TODO: Need to implement charting')
        ];
    }
}

class InputComponent {
    constructor(vnode) {}

    validateWeight(event) {
        event.preventDefault();
        let new_weight_input = document.querySelector('#new_weight');
        let new_body_weight = parseFloat(new_weight_input.value);
        if (isNaN(new_body_weight)) {
            window.alert("Invalid weight -- must be a number");
        } else {
            fetch("weight.php", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ weight: new_body_weight })
            })
            .then((res) => res.json())
            .then((data) => {
                if (data.result === "ok") {
                    window.alert("New weight successfully added!");
                    document.querySelector('input#new_weight').value = "";
                }
                else window.alert("An error has occurred");
            })
            .catch((error) => {
                window.alert(error.json());
            });
        }
    }

    view(vnode) {
        return [
            m('.box', [
                m('form', {
                    onsubmit: () => { 
                        this.validateWeight(event); 
                    }
                }, [
                    m('.field', [
                        m('label.label', 'New Body Weight (lbs.)'),
                        m('.control', [
                            m('input#new_weight.input[type=number]', {
                                placeholder: '135',
                                min: 100,
                                max: 300,
                                step: .1
                            })
                        ])
                    ]),
                    m('input[type=submit][value="Add New Weight"].button.is-primary.is-fullwidth')
                ])
            ])
        ];
    }
}

class BodyWeight {
    view(vnode) {
        console.log("this is being called...");
        return m('li', { id: vnode.attrs.model.id }, `${vnode.attrs.model.date_logged} - ${vnode.attrs.model.weight}`)
    }
}

class HistoryLayout {
    constructor(vnode) {
        this.weights = [];
    }

    oninit() {
        fetch('weight.php')
        .then((response) => response.json())
        .then((weights_json) => {
            this.weights = weights_json;
        });
    }

    view(vnode) {
        return m('section.section', 
            m('ul', this.weights.map((weight) => {
                // return m('li', { id: weight.id}, `${weight.date_logged} - ${weight.weight}`);
                return m(BodyWeight, {key: weight.id, model: weight})
            }))
            // m('table.table.is-striped.is-bordered.is-fullwidth', [
            //     m('thead', [
            //         m('tr', [
            //             m('th', 'Date'),
            //             m('th', 'Weight')
            //         ])
            //     ]), // what's causing the fail? oninit is being called...
            //     m('tbody', this.weights.map((weight) => {
            //         return m('tr', { key: weight.id }, [
            //             m('td', weight.date_logged),
            //             m('td', weight.weight),
            //         ]);
            //     })),
            // ])
        );
    }
}

module.exports = { layout: WeightLayout, chart: ProgressChart }
