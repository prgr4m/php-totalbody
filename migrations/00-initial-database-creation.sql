-- 00-initial-database-creation.sql
CREATE TABLE IF NOT EXISTS weight_logs (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    weight DECIMAL NOT NULL,
    date_logged DATE DEFAULT CURRENT_DATE
);
