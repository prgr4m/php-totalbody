<?php declare(strict_types=1);
require_once __DIR__ . '/../vendor/autoload.php';

$templates_dir = __DIR__ . '/../templates';
$templates = new League\Plates\Engine($templates_dir);
$templates->addFolder('layouts', $templates_dir . '/layouts');

echo $templates->render('index');
