<?php 
require_once __DIR__ . '/../vendor/autoload.php';
use TotalBody\Db\WeightManager;


$weight_manager = new WeightManager();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    echo json_encode($weight_manager->get_all_weight()->fetchAll(PDO::FETCH_ASSOC));
} elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['weight'])) {
        $weight_manager->add_new_weight($_POST['weight']);
    } else { // $HTTP_RAW_POST_DATA | php://input
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body);
        $weight_manager->add_new_weight($data->weight);
    }
    echo json_encode(['result'=>'ok']);
} else {
    http_response_code(403);
}
