<?php
namespace TotalBody\Db;

const DB_PATH = __DIR__ . "/../../var/db/totalbody.db";

class DbManager {
    private static $_connection = null;

    private function __construct() { // shouldn't even go here!
        throw new \Exception("Db class cannot be instantiated! Call Db::get_connection()"); 
    }

    static function get_connection(): \PDO {
        if (is_null(self::$_connection)) {
            try {
                $dbh = new \PDO("sqlite:" . DB_PATH); // set return type options?
                self::$_connection = $dbh;
            } catch (\PDOException $e) {
                echo "Cannot create a database connection failed: $e->getMessage()\n";
            }
        } return self::$_connection;
    }
}
