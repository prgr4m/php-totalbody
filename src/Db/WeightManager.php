<?php 
namespace TotalBody\Db;

use TotalBody\Db\DbManager;


class WeightManager {
    private $_dbh;

    public function __construct() {
        $this->_dbh = DbManager::get_connection();
    }

    public function add_new_weight(float $weight=0.0): void {
        $sanitized_bodyweight = filter_var($weight, FILTER_SANITIZE_NUMBER_FLOAT, ['flags' => FILTER_FLAG_ALLOW_FRACTION]);
        if (!empty($sanitized_bodyweight)) {
            if ($stmt = $this->_dbh->prepare('insert into weight_logs (weight) values (:new_value)')) {
                $stmt->bindParam(':new_value', $sanitized_bodyweight);
                $stmt->execute();
            } else throw new \Exception("Could not write new bodyweight value into database");
        }
    }

    public function get_all_weight(): \PDOStatement {
        return $this->_dbh->query("select * from weight_logs");
    }
}
