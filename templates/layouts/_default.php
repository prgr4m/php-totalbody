<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <title><?=$this->e($title)?></title>
    <link rel="stylesheet" href="css/app.css" type="text/css" media="all">
</head>
<body>
<?=$this->section('content')?>
</body>
</html>
