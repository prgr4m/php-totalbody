<?php 
use PHPUnit\Framework\TestCase;
use TotalBody\Db\DbManager;

class DbTest extends TestCase {
    public function test_db_instance() {
        $this->assertInstanceOf(\PDO::class, DbManager::get_instance());
    }
}
